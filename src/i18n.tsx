import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import * as sk from './locales/sk.json';
import * as en from './locales/en.json';

i18n
  .use(initReactI18next)
  .init({
    resources: {
      en: {
        translation: en
      },
      sk: {
        translation: sk
      }
    },
    lng: 'sk',
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;
