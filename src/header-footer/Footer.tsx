import './Footer.scss';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import { useTranslation } from 'react-i18next';

export function Footer() {
  const { t } = useTranslation();

  return (
    <Navbar bg="light" fixed="bottom">
      <Container className="p-1 justify-content-center">
        {t('footer')}
      </Container>
    </Navbar>
  );
}
