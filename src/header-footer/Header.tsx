import './Header.scss';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container';
import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function Header() {
  const location = useLocation();
  const { i18n, t } = useTranslation();

  return (
    <Navbar bg="light" expand="sm" sticky="top">
      <Container>
        <Navbar.Brand href="#home">LOGO</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto" activeKey={location.pathname}>

            <Nav.Link as={Link} href="/" to="/">
              {t('notes')}
            </Nav.Link>
            <Nav.Link as={Link} href="/about" to="/about">
              {t('about')}
            </Nav.Link>

            <NavDropdown title={i18n.language} id="language">
              {Object.keys(i18n.services.resourceStore.data).map(lang =>
                <NavDropdown.Item
                  key={lang}
                  onClick={() => i18n.changeLanguage(lang)}
                >
                  {lang}
                </NavDropdown.Item>
              )}
            </NavDropdown>

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
