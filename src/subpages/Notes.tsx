import './Notes.scss';
import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';
import Toast from 'react-bootstrap/Toast';
import { useTranslation } from 'react-i18next';
import { Note } from '../services/types';
import { createNote, deleteNote, getNotes, updateNote } from '../services/notesService';
import { SubmitHandler, useForm } from 'react-hook-form';
import { BsCheck, BsFillTrashFill } from 'react-icons/bs';
import { MdAccessTime } from 'react-icons/md';

function setWithout<T>(set: Set<T>, item: T): Set<T> {
  const after = new Set<T>(set);
  after.delete(item);
  return after;
}

type CreateFormValues = {
  note: string;
};


export default function Notes() {
  const { t } = useTranslation();

  const { register, reset, handleSubmit, formState: { errors, isValid } } = useForm<CreateFormValues>({ mode: 'onChange' });

  const [loading, setLoading] = useState(true);
  const [notes, setNotes] = useState<Note[]>([]);
  const [error, setError] = useState<string | null>(null);
  const [working, setWorking] = useState<Set<number>>(new Set()); // Id of notes that have some request pending.

  useEffect(() => {
    getNotes()
      .then(notes => {
        setNotes(notes);
        setLoading(false);
      }).catch(() => {
        setLoading(false);
        setError(t('notes.get_list_error'));
      });
  }, []);

  // Optimistic create
  const onSubmitCreateForm: SubmitHandler<CreateFormValues> = data => {
    reset();
    const note: Note = {
      id: -Date.now(), // Server ignores any id sent and will assign a new one in the result.
      note: data.note,
      done: false
    }
    setWorking(working => new Set(working).add(note.id));
    setNotes(notes => [...notes, note]);
    createNote(note).then(newNote => {
      setWorking(working => setWithout(working, note.id));
      setNotes(notes => notes.map(n => n.id === note.id ? newNote : n));
    }).catch(() => {
      setWorking(working => setWithout(working, note.id));
      setNotes(notes => notes.filter(n => n.id !== note.id));
      setError(t('notes.create_error'));
    });
  };

  // Pessimistic update
  const handleUpdateNote = (note: Note) => {
    setWorking(working => new Set(working).add(note.id));
    updateNote({
      ...note,
      done: true
    }).then(newNote => {
      setWorking(working => setWithout(working, note.id));
      setNotes(notes => notes.map(n => n.id === note.id ? newNote : n));
    }).catch(() => {
      setWorking(working => setWithout(working, note.id));
      setError(t('notes.update_error'));
    });
  };

  // Pessimistic delete
  const handleDeleteNote = (note: Note) => {
    setWorking(working => new Set(working).add(note.id));
    deleteNote(note.id)
      .then(() => {
        setWorking(working => setWithout(working, note.id));
        setNotes(notes => notes.filter(n => n.id !== note.id));
      }).catch(() => {
        setWorking(working => setWithout(working, note.id));
        setError(t('notes.delete_error'));
      });
  };

  return (
    <Container className="notes">
      <h2>{t('notes')}</h2>

      <Form onSubmit={handleSubmit(onSubmitCreateForm)}>
        <Form.Control
          type="input"
          placeholder={t('notes.create_placeholder')}
          {...register('note', { required: true, maxLength: 256 })}
        />
        {errors?.note?.type === 'required' && (
          <div className="text-danger">{t('notes.note_validation_required')}</div>
        )}
        {errors?.note?.type === 'maxLength' && (
          <div className="text-danger">{t('notes.note_validation_long').replace('{0}', '256')}</div>
        )}
        <Button
          variant="primary"
          type="submit"
          disabled={!isValid}
        >
          {t('notes.action_create')}
        </Button>
      </Form>

      <Table responsive borderless>
        <tbody>
          {notes.map(note => (
            <tr key={note.id}>
              <td className={note.done ? 'done' : ''}>
                {note.note}
              </td>
              <td>
                {working.has(note.id) ? (
                  <MdAccessTime title={t('notes.action_working')} />
                ) : (
                  <a href="#" onClick={() => note.done ? handleDeleteNote(note) : handleUpdateNote(note)}                  >
                    {note.done
                      ? <BsFillTrashFill title={t('notes.action_delete')} />
                      : <BsCheck title={t('notes.action_finish')} />
                    }
                  </a>
                )}
              </td>
            </tr>
          ))}

          {loading
            ? <tr><td>{t('notes.loading_list')}</td></tr>
            : (notes.length === 0 && <tr><td>{t('notes.empty_list')}</td></tr>)
          }
        </tbody>
      </Table>

      <Toast
        className="position-absolute start-0 bottom-0 mb-5"
        bg="danger"
        show={error !== null}
        onClose={() => setError(null)}
        delay={5000}
        autohide
      >
        <Toast.Header className="justify-content-between">
          {error}
        </Toast.Header>
      </Toast>
    </Container>
  );
}
