import React from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { Trans, useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

export default function About() {
  const { t } = useTranslation();

  return (
    <Container>
      <main>
        <h2>{t('about')}</h2>
        <Trans i18nKey="lorem_ipsum" />
      </main>
      <nav>
        <Link to="/">
          <Button variant="primary">
            {t('go_to_notes')}
          </Button>
        </Link>
      </nav>
    </Container>
  );
}
