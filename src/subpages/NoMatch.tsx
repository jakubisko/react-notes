import React from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';

export const NoMatch = () => (
  <Container>
    <main>
      <h2>Oops! Such page does not exists!</h2>
    </main>
    <nav>
      <Link to="/">
        <Button variant="primary">
          Home
        </Button>
      </Link>
    </nav>
  </Container>
);
