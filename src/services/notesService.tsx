import { Note } from './types';

export async function getNotes(): Promise<Note[]> {
  const resp = await fetch(process.env.REACT_APP_BACKEND_URL + '/notes');
  if (!resp.ok) {
    throw new Error('Failed to retrieve list of notes.');
  }
  return resp.json();
}

export async function createNote(note: Note): Promise<Note> {
  const resp = await fetch(process.env.REACT_APP_BACKEND_URL + '/notes', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(note)
  });
  if (!resp.ok) {
    throw new Error('Failed to create note.');
  }
  return resp.json();
}

export async function updateNote(note: Note): Promise<Note> {
  const resp = await fetch(process.env.REACT_APP_BACKEND_URL + '/notes/' + note.id, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(note)
  });
  if (!resp.ok) {
    throw new Error('Failed to update note.');
  }
  return resp.json();
}

export async function deleteNote(noteId: number): Promise<void> {
  const resp = await fetch(process.env.REACT_APP_BACKEND_URL + '/notes/' + noteId, {
    method: 'DELETE'
  });
  if (!resp.ok) {
    throw new Error('Failed to delete note.');
  }
}
