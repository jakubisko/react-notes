export interface Note {
  id: number;
  note: string;
  done: boolean;
}
