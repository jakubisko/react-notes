import React from 'react';
import { Route, Routes } from 'react-router';
import Notes from './subpages/Notes';
import About from './subpages/About';
import Header from './header-footer/Header';
import { Footer } from './header-footer/Footer';
import { NoMatch } from './subpages/NoMatch';

export default function App() {
  return (
    <div className="text-center">
      <Header />

      <Routes>
        <Route path="/" element={<Notes />} />
        <Route path="about" element={<About />} />
        <Route path="*" element={<NoMatch />} />
      </Routes>

      <Footer />
    </div>
  );
}
