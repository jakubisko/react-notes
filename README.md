# Notes test application

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `npm i`

Installs project dependencies.

### `json-server --watch db.json --delay 500 --port 3004`

Runs local mock server for testing purposes which holds database in-memory.
This requires `json-server` to be global installed globally.
You can do that by running `npm install -g json-server` once.

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm run build`

Builds the app for production to the `build` folder.

## Modifying the theme

You can customize this project's theme as any other project that uses Bootstrap.
The best way to customize Bootstrap is to override its variables. Place any variable overrides in the `theme-colors.scss` file.
You can find the list of all Bootstrap 5 variables here. The list can be filtered with 'search' field at the top right:
https://rstudio.github.io/bslib/articles/bs5-variables.html

As an example, to change the primary color (used by buttons) to green, add
```
$primary: green;
```

To change the color of the text in the body to brown, add
```
$body-color: brown;
```

## Libraries used

* react-router-dom - Client-side routing.
https://reactrouter.com/

* Bootstrap - Front-end framework for developing and styling responsive, mobile first projects on the web.
https://getbootstrap.com/
* React Bootstrap - Integration of the above library with React.
https://react-bootstrap.github.io/

* react-icons - Free icons for React
https://www.npmjs.com/package/react-icons

* i18next - Internationalization framework.
https://www.npmjs.com/package/i18next
* react-i18next - Integration of the above library with React.
https://react.i18next.com/

* react-hook-form - Form validation.
https://react-hook-form.com/

* json-server - Fake REST API server for mocking backend.
https://github.com/typicode/json-server
